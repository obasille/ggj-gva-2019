// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "MRO_Shader"
{
	Properties
	{
		_Loft_Pack_1_DIFF("Loft_Pack_1_DIFF", 2D) = "white" {}
		_Loft_Pack_1_MRO("Loft_Pack_1_MRO", 2D) = "white" {}
		_Loft_Pack_1_NORM("Loft_Pack_1_NORM", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Loft_Pack_1_NORM;
		uniform float4 _Loft_Pack_1_NORM_ST;
		uniform sampler2D _Loft_Pack_1_DIFF;
		uniform float4 _Loft_Pack_1_DIFF_ST;
		uniform sampler2D _Loft_Pack_1_MRO;
		uniform float4 _Loft_Pack_1_MRO_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Loft_Pack_1_NORM = i.uv_texcoord * _Loft_Pack_1_NORM_ST.xy + _Loft_Pack_1_NORM_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Loft_Pack_1_NORM, uv_Loft_Pack_1_NORM ) );
			float2 uv_Loft_Pack_1_DIFF = i.uv_texcoord * _Loft_Pack_1_DIFF_ST.xy + _Loft_Pack_1_DIFF_ST.zw;
			o.Albedo = tex2D( _Loft_Pack_1_DIFF, uv_Loft_Pack_1_DIFF ).rgb;
			float2 uv_Loft_Pack_1_MRO = i.uv_texcoord * _Loft_Pack_1_MRO_ST.xy + _Loft_Pack_1_MRO_ST.zw;
			float4 tex2DNode2 = tex2D( _Loft_Pack_1_MRO, uv_Loft_Pack_1_MRO );
			o.Metallic = tex2DNode2.r;
			o.Smoothness = tex2DNode2.g;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14001
815;687;1638;742;421;-41;1;True;True
Node;AmplifyShaderEditor.SamplerNode;3;180,433;Float;True;Property;_Loft_Pack_1_NORM;Loft_Pack_1_NORM;2;0;Assets/LADPM_Assets/Environement/Loft/Textures/Loft_Pack_1_NORM.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;189,27;Float;True;Property;_Loft_Pack_1_DIFF;Loft_Pack_1_DIFF;0;0;Assets/LADPM_Assets/Environement/Loft/Textures/Loft_Pack_1_DIFF.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;186,220;Float;True;Property;_Loft_Pack_1_MRO;Loft_Pack_1_MRO;1;0;Assets/LADPM_Assets/Environement/Loft/Textures/Loft_Pack_1_MRO.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;778,33;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;MRO_Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;0;0;1;0
WireConnection;0;1;3;0
WireConnection;0;3;2;1
WireConnection;0;4;2;2
ASEEND*/
//CHKSM=978C5EC6C5A520450E2293468FDDD28A3B00AD12