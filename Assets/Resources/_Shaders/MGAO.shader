// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "MGAO"
{
	Properties
	{
		_T_Dwn_Table_DIFF("T_Dwn_Table_DIFF", 2D) = "white" {}
		_T_Dwn_Table_MRO("T_Dwn_Table_MRO", 2D) = "white" {}
		_T_Dwn_Table_NORM("T_Dwn_Table_NORM", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _T_Dwn_Table_NORM;
		uniform float4 _T_Dwn_Table_NORM_ST;
		uniform sampler2D _T_Dwn_Table_DIFF;
		uniform float4 _T_Dwn_Table_DIFF_ST;
		uniform sampler2D _T_Dwn_Table_MRO;
		uniform float4 _T_Dwn_Table_MRO_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_T_Dwn_Table_NORM = i.uv_texcoord * _T_Dwn_Table_NORM_ST.xy + _T_Dwn_Table_NORM_ST.zw;
			o.Normal = tex2D( _T_Dwn_Table_NORM, uv_T_Dwn_Table_NORM ).rgb;
			float2 uv_T_Dwn_Table_DIFF = i.uv_texcoord * _T_Dwn_Table_DIFF_ST.xy + _T_Dwn_Table_DIFF_ST.zw;
			o.Albedo = tex2D( _T_Dwn_Table_DIFF, uv_T_Dwn_Table_DIFF ).rgb;
			float2 uv_T_Dwn_Table_MRO = i.uv_texcoord * _T_Dwn_Table_MRO_ST.xy + _T_Dwn_Table_MRO_ST.zw;
			float4 tex2DNode2 = tex2D( _T_Dwn_Table_MRO, uv_T_Dwn_Table_MRO );
			o.Metallic = tex2DNode2.r;
			o.Smoothness = tex2DNode2.g;
			o.Occlusion = tex2DNode2.b;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "MGAO"
}
/*ASEBEGIN
Version=16200
265.6;177.6;1020;720;571.1662;331.4327;1.180583;True;True
Node;AmplifyShaderEditor.SamplerNode;2;-325,123;Float;True;Property;_T_Dwn_Table_MRO;T_Dwn_Table_MRO;1;0;Create;True;0;0;False;0;1c66746c76a9d1344a72a5e7d0b8f0e9;1c66746c76a9d1344a72a5e7d0b8f0e9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;3;-303,-73;Float;True;Property;_T_Dwn_Table_NORM;T_Dwn_Table_NORM;2;0;Create;True;0;0;False;0;9bee1aa278450ca4c88e27826feb3ba5;9bee1aa278450ca4c88e27826feb3ba5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-308,-261;Float;True;Property;_T_Dwn_Table_DIFF;T_Dwn_Table_DIFF;0;0;Create;True;0;0;False;0;e607e186a168ae24699821efd2241808;e607e186a168ae24699821efd2241808;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;159,-214;Float;False;True;2;Float;MGAO;0;0;Standard;MGAO;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;0;0;1;0
WireConnection;0;1;3;0
WireConnection;0;3;2;1
WireConnection;0;4;2;2
WireConnection;0;5;2;3
ASEEND*/
//CHKSM=340C65A91FCC2EBAD02181EA8167C93D0BF04EF5