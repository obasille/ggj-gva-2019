﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum ShadowState {Patrol, Stalk, Attack};
public class Shadow : MonoBehaviour
{
    public Transform[] waypoints;
    private int waypointIndex = 0;
    
    public float movementSpeed = 1f;

    public float movementWaitTime = 3f;
    public float stalkDistance = 15f;

    public float timeBeforeAttack = 3f;

    public Transform currentWaypoint => waypoints[waypointIndex];

    Color stateColor = Color.black;

    PlayerController player;

    ShadowState currentState;

    Cooldown movementCooldown = new Cooldown();
    Cooldown stalkCooldown = new Cooldown();

    Cooldown attackCooldown = new Cooldown();

    float playerDistance => player != null ? Vector3.Distance(transform.position, player.transform.position) : Mathf.Infinity;

    SoundPlayer soundPlayer;
    
    DragonBones.UnityArmatureComponent armatureComponent;

    void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        soundPlayer = GetComponent<SoundPlayer>();
        armatureComponent = transform.GetComponentInChildren<DragonBones.UnityArmatureComponent>();
        ResetPosition();
    }

    void ResetPosition()
    {
        if(waypoints.Length > 0)
        {
            transform.position = waypoints[0].position;
        }
    }

    void OnEnable()
    {
        AutoLoadScenes.SceneLoaded += OnSceneLoaded;
        PlayerController.LevelExit += OnLevelExit;
        PlayerController.PeeMaxed += OnPeeMaxed;

    }

    void OnDisable()
    {
        AutoLoadScenes.SceneLoaded -= OnSceneLoaded;
        PlayerController.LevelExit -= OnLevelExit;
        PlayerController.PeeMaxed -= OnPeeMaxed;
    }

    void Start()
    {
        movementCooldown.Reset(movementWaitTime);
    }

    // Update is called once per frame
    void Update()
    {
        switch(currentState)   
        {
            case ShadowState.Patrol:
                UpdatePatrol();
            break;
            case ShadowState.Stalk:
                UpdateStalk();
            break;
            case ShadowState.Attack:
                UpdateAttack();
            break;
        }        
    }

    void SwitchState(ShadowState newState)
    {
        currentState = newState;
    }

    void UpdatePatrol()
    {
        if(movementCooldown.UpdateCooldown())
        {           
            Vector3 previousPosition = currentWaypoint.position;
            waypointIndex++;
            if(waypointIndex >= waypoints.Length)
            {
                waypointIndex = 0;
            }
            float distance = Vector3.Distance(previousPosition, currentWaypoint.position);
            float travelDuration = distance / movementSpeed;
            soundPlayer.Play("Grogne");
            armatureComponent.animation.Play("Run", -1);
            transform.DOMove(waypoints[waypointIndex].position, travelDuration).OnComplete(() => {
                movementCooldown.Reset(movementWaitTime); 
                armatureComponent.animation.Play("Idle", -1);
            });
            
        } else {
            if(!player.isUnderCover && playerDistance < stalkDistance)
            {
                SwitchState(ShadowState.Stalk);
                armatureComponent.animation.Play("Repéré", -1);
                soundPlayer.Play("Stalk", true);
                stalkCooldown.Reset(timeBeforeAttack);
            }
        }
        
    }

    void UpdateStalk()
    {
        if(player.isUnderCover)
        {
            SwitchState(ShadowState.Patrol);
        }

        if(stalkCooldown.UpdateCooldown())
        {
            SwitchState(ShadowState.Attack);
            armatureComponent.animation.Play("Gotcha", -1);
            soundPlayer.Play("Hurl");
            attackCooldown.Reset(.1f);
        } else {
            if(playerDistance > stalkDistance + 2f)
            {
                SwitchState(ShadowState.Patrol);   
                soundPlayer.Stop();             
            }
        }
    }

    void UpdateAttack()
    {
       
        if(attackCooldown.UpdateCooldown())
        {
            transform.DOMove(player.transform.position + player.transform.forward + Vector3.down, 1f).OnComplete(() => {                
                ResetPosition();
                SwitchState(ShadowState.Patrol);
                player.IncreaseFear(.1f);
            });
        }
    }

    void OnDrawGizmos()
    {
        if(waypoints != null)
        {
            Gizmos.color = Color.yellow;
            for(int i = 0; i < waypoints.Length-1; i++)
            {          
                Gizmos.DrawLine(waypoints[i].position, waypoints[i+1].position);
            }

            Gizmos.DrawWireSphere(waypoints[0].position, stalkDistance);
        }
        

        if(Application.isPlaying)
        {
            switch(currentState)   
            {
                case ShadowState.Patrol:
                    Gizmos.color = Color.green;
                break;
                case ShadowState.Stalk:
                    Gizmos.color = Color.yellow;
                break;
                case ShadowState.Attack:
                    Gizmos.color = Color.red;
                break;
            }
            Gizmos.DrawCube(transform.position + Vector3.up * 3f, Vector3.one);
        }
        
    }

    void OnSceneLoaded()
    {
        player = GameObject.FindObjectOfType<PlayerController>();        
    }

    void OnLevelExit()
    {
        Destroy(gameObject);
    }

    void OnPeeMaxed()
    {
        Destroy(gameObject);
    }
}
