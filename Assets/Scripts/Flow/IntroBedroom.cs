﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class IntroBedroom : MonoBehaviour
{
    PlayableDirector director;
    bool sceneFinished = false;

    void Awake()
    {
        director = GetComponent<PlayableDirector>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(director.state != PlayState.Playing && sceneFinished == false)
        {
            sceneFinished = true;
            Debug.Log("Scene finished");
            SceneManager.LoadScene("Environment");
        }
    }
}
