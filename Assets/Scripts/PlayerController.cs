﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Rendering.PostProcessing;

public class PlayerController : MonoBehaviour
{
    private Player player;
    CharacterController character;
    Transform cameraRig;

    Vector3 movementDirection = Vector3.zero;
    Vector3 lookRotation = Vector3.zero;
    float lookHorizontal;
    float lookVertical = .5f;

    public Vector2 lookSpeed;

    public float lookVerticalMax;
    public float lookVerticalMin;

    public float peeLevel = 0;
    public float fearLevel = 0;
    public float moveSpeed = 20f;    
    public bool isRunning = false;

    public float effectiveMoveSpeed => isRunning ? moveSpeed * 2f : moveSpeed;
    float distanceTraveled;
    float lastFootstepDistance;
    public bool isUnderCover {get; private set;} = false;

    bool hasExitedLevel = false;

    Cooldown footstepCooldown = new Cooldown();
    SoundPlayer soundPlayer;

    PostProcessVolume fearVolume;

    bool isDead = false;

    public static System.Action PeeMaxed;
    public static System.Action FearMaxed;

    public static System.Action LevelExit;

    public static System.Action PlayerHit;

    bool runningStarted = false;

    Cooldown pantingCooldown = new Cooldown();

    // Start is called before the first frame update
    void Awake()
    {
        character = GetComponent<CharacterController>();    
        cameraRig = transform.Find("CameraRig");
        player = ReInput.players.GetPlayer(0);
        lookVertical = Mathf.InverseLerp(lookVerticalMin, lookVerticalMax, 0);
        soundPlayer = GetComponent<SoundPlayer>();
        footstepCooldown.Reset(.5f);
        fearVolume = transform.Find("FearVolume").GetComponent<PostProcessVolume>();
    }

    // Update is called once per frame
    void Update()
    {        
        if(!isDead && !hasExitedLevel)
        {
            bool wasRunning = isRunning;
            Inputs();            
            Movement();

            if(isRunning && wasRunning != isRunning)
            {
                runningStarted = true;                
            }

            runningStarted = isRunning ? runningStarted : false;
        }

        if(runningStarted && movementDirection.magnitude > 0)
        {
            if(!pantingCooldown.IsActive)
            {
                soundPlayer.Play("Panting");
                pantingCooldown.Reset(3f);
            }            
            runningStarted = false;
        }

        pantingCooldown.UpdateCooldown();

        if(!isUnderCover)
        {
            peeLevel += (.0025f + (.025f * fearLevel)) * (isRunning ? 2f : 1f ) * Time.deltaTime;
        } else {
            fearLevel -= .1f * Time.deltaTime;
        }

        peeLevel = Mathf.Clamp01(peeLevel);
        fearLevel = Mathf.Clamp01(fearLevel);

        if(distanceTraveled - lastFootstepDistance > 3f)
        {
            lastFootstepDistance = distanceTraveled;            
            soundPlayer.Play("Footstep");        
        }

        fearVolume.weight = fearLevel;

        if(isDead == false && peeLevel >= 1f)
        {
            isDead = true;
            PeeMaxed?.Invoke();
        }
    }

    void Movement()
    {
        character.SimpleMove(transform.TransformDirection(movementDirection) * effectiveMoveSpeed);

        distanceTraveled += effectiveMoveSpeed * Time.deltaTime * movementDirection.magnitude;

        transform.Rotate(new Vector3(0, lookHorizontal, 0) * Time.deltaTime * lookSpeed.y);

        cameraRig.localRotation = Quaternion.Lerp(Quaternion.Euler(lookVerticalMin,0,0), Quaternion.Euler(lookVerticalMax,0,0), lookVertical);
    }
    
    void Inputs()
    {
        movementDirection.x = player.GetAxis("Move Horizontal");
        movementDirection.z = player.GetAxis("Move Vertical");        

        lookHorizontal = player.GetAxis("Look Horizontal");
        lookVertical += player.GetAxis("Look Vertical") * Time.deltaTime * lookSpeed.x;

        lookVertical = Mathf.Clamp01(lookVertical);

        isRunning = player.GetButton("Run");
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position + Vector3.up * 5f, 1f);
        Gizmos.DrawCube(transform.position + Vector3.up + transform.forward, Vector3.one);
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("PillowFort"))
        {
            isUnderCover = true;
            Debug.Log("Got under cover");
        }

        if(collider.CompareTag("LevelExit") && !hasExitedLevel)
        {
            hasExitedLevel = true;
            LevelExit?.Invoke();
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if(collider.CompareTag("PillowFort"))
        {
            isUnderCover = false;
            Debug.Log("Left cover");
        }
    }

    public void IncreaseFear(float delta)
    {
        fearLevel += delta;
        PlayerHit?.Invoke();
    }
}
