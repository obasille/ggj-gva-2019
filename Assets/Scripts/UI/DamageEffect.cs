﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;
public class DamageEffect : MonoBehaviour
{
    PostProcessVolume postProcessVolume;
    Tween hitAnimation;
    void OnEnable()
    {
        PlayerController.PlayerHit += OnSpaceshipHit;
    }

    void OnDisable()
    {
        PlayerController.PlayerHit -= OnSpaceshipHit;
    }
    void Awake()
    {
        postProcessVolume = GetComponent<PostProcessVolume>();
        postProcessVolume.weight = 0;

        
    }

    void OnSpaceshipHit()
    {        
        hitAnimation.Restart();
    }

    void Start()
    {
        hitAnimation = DOTween.To(() => 1f, x => postProcessVolume.weight = x, 0, 1f).SetAutoKill(false).Pause();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
