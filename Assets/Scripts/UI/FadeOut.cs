﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeOut : MonoBehaviour
{
    IEnumerator Start()
    {
        var img = GetComponent<Image>();
        img.color = Color.black;
        yield return new WaitForSeconds(1);
        DOTween.To(() => img.color.a, x => img.color = new Color(0, 0, 0, x), 0, 1).OnComplete(() => DestroyImmediate(gameObject));
    }
}
