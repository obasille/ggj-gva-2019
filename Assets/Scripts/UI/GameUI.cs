﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
    Image peeValue;
    Image fearValue;

    PlayerController player;

    CanvasGroup gameoverPanel;
    CanvasGroup winPanel;

    void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        peeValue = transform.Find("Hud/PeeBackground/PeeValue").GetComponent<Image>();
        fearValue = transform.Find("Hud/FearBackground/FearValue").GetComponent<Image>();
        gameoverPanel = transform.Find("Panels/GameoverPanel").GetComponent<CanvasGroup>();
        winPanel = transform.Find("Panels/WinPanel").GetComponent<CanvasGroup>();
    }
    void Start()
    {
        // yield return new WaitForSeconds(2);
        // OnWin();
    }

    void OnEnable()
    {
        PlayerController.PeeMaxed += OnPeeMaxed;
        PlayerController.LevelExit += OnWin;
    }

    void OnDisable()
    {
        PlayerController.PeeMaxed -= OnPeeMaxed;
        PlayerController.LevelExit -= OnWin;
    }

    void OnPeeMaxed()
    {   
        gameoverPanel.gameObject.SetActive(true);
        var ovr = gameoverPanel.transform.Find("Ovr");
        ovr.DOMoveY(0, 10).SetDelay(1);
        StartCoroutine(OnDelayLoadStart(15));
    }
    
    void OnWin()
    {   
        winPanel.gameObject.SetActive(true);
        StartCoroutine(OnDelayLoadStart(10));
    }
    
    IEnumerator OnDelayLoadStart(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("Start");
    }

    // Update is called once per frame
    void Update()
    {
        peeValue.fillAmount = player.peeLevel;
        fearValue.fillAmount = player.fearLevel;
    }
}
