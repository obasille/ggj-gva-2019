﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MessagePanel : MonoBehaviour
{
    CanvasGroup canvasGroup;

    public System.Action MessagePanelShow;
    public System.Action MessagePanelHide;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void OnEnable()
    {   
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;     
        canvasGroup.alpha = 0;
        Button[] buttons = transform.GetComponentsInChildren<Button>();
        
        if(buttons.Length > 0)
        {
            buttons[0].Select();
        }

        DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, 1f, 1f).OnComplete(() => {
            
        });

        MessagePanelShow?.Invoke();
    }

    public void Deactivate()
    {
        if(gameObject.activeSelf)
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            gameObject.SetActive(false);
            MessagePanelHide?.Invoke();
        }
    }
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
