﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnable()
    {
        StartCoroutine(WinSequence());
    }
    IEnumerator WinSequence()
    {
        yield return new WaitForSeconds(1f);
        GetComponent<SoundPlayer>().Play("Soulage");

        yield return new WaitForSeconds(3f);

        GetComponent<SoundPlayer>().Play("Flush");
    }
}
