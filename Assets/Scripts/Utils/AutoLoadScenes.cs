﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoLoadScenes : MonoBehaviour
{
    public string[] Scenes;

    static AutoLoadScenes instance;

    public static System.Action SceneLoaded;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (instance == this)
        {
            StartCoroutine(SceneLoadCheck());
            DontDestroyOnLoad(this);
            var missingScenes = Scenes.Except(
                     Enumerable.Range(0, SceneManager.sceneCount)
                     .Select(i => SceneManager.GetSceneAt(i).name));
            foreach (var sceneName in missingScenes)
            {
                Debug.Log("Auto loading: " + sceneName);
                SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator SceneLoadCheck()
    {
        while(SceneManager.sceneCount < Scenes.Length)
        {
            yield return null;
        }

        SceneLoaded?.Invoke();
        Debug.Log("All scenes loaded");

        while(Enumerable.Range(0, SceneManager.sceneCount)
                .Any(i => SceneManager.GetSceneAt(i).name == Scenes[0]))
        {
            yield return null;
        }

        Destroy(gameObject);
        instance = null;
        Debug.Log("All scenes unloaded");
    }
}
