﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooldown {

	private float previousValue;
	public float value;

	public float totalTime;

	public bool IsActive = false;

	public float completion => (totalTime - Mathf.Max(0,value)) / Mathf.Max(.01f, totalTime);
	
	// Update is called once per frame
	public bool UpdateCooldown () {		
		if(value > 0)
		{
			value -= Time.deltaTime;
			if(value <= 0 && IsActive)
			{
				previousValue = value;
				IsActive = false;
				return true;
			}
			previousValue = value;
		}		
		return false;
	}

	public void Reset(float cooldownValue)
	{
		value = cooldownValue;
		totalTime = cooldownValue;
		IsActive = true;
	}

	public void Stop()
	{
		IsActive = false;
	}
}