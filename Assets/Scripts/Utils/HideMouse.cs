﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideMouse : MonoBehaviour
{
    void OnEnable()
    {
        if (!Application.isEditor)
        {
            Cursor.visible = false;
        }
    }

    void OnDisable()
    {
        if (!Application.isEditor)
        {
            Cursor.visible = true;
        }
    }
}
