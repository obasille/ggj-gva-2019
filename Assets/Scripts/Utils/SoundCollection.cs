﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundClip
{
	public string name;
	public AudioClip[] clips;
	
	[Range(-3, 3f)]
	public float minPitch = 1f;

	[Range(-3, 3f)]
	public float maxPitch = 1f;
	public bool loop = false;
	public bool randomPitch;
}


[CreateAssetMenu(menuName="SoundCollection")]
public class SoundCollection : ScriptableObject {
	public SoundClip[] soundClips;
	
}
