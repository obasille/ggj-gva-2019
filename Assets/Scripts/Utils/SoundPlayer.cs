﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour {

	private AudioSource source;
	public SoundCollection soundCollection;
	
	public Dictionary<string, SoundClip> indexedClips = new Dictionary<string, SoundClip>();
	
	public void Awake()
	{
		source = GetComponent<AudioSource>();
		if(soundCollection == null)
		{
			Debug.LogWarning($"No sound collection for object {gameObject.name}");
		} else {
			foreach(SoundClip clip in soundCollection.soundClips)
			{
			indexedClips.Add(clip.name, clip);
			}
		}		
	}
	public void Play(string name, bool forceSource = false)
	{								
		if(!indexedClips.ContainsKey(name))
		{
			Debug.LogWarning($"No clip named {name} found");
			return;
		}
		SoundClip soundClip = indexedClips[name];

		AudioClip finalClip;
		if(soundClip.clips.Length > 1)
		{
			finalClip = soundClip.clips[Random.Range(0,soundClip.clips.Length)];
		} else {
			finalClip = soundClip.clips[0];
		}

		source.pitch = 1f;
		
		if(soundClip.minPitch != soundClip.maxPitch)
		{
			source.clip = finalClip;
			source.pitch = Random.Range(soundClip.minPitch, soundClip.maxPitch);
			source.Play();
		} else {
			if(!forceSource)
			{
				source.PlayOneShot(finalClip);
			} else {
				source.clip = finalClip;
				source.Play();
			}
		}		
		
		//source.clip = soundClip.clips[0];
		
	}
	
	public void Stop()
	{
		source.Stop();
	}

}
