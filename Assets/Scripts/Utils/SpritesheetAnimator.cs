﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public enum SpritesheetAnimationPlayback {Loop, Wrap};

[System.Serializable]
public class SpritesheetAnimation
{
    public string name;
    public int frameCount => frameDurations.Length;

    public SpritesheetAnimationPlayback playback;
    public float[] frameDurations;

}

public class SpritesheetFrame
{

}

public class SpritesheetAnimator : MonoBehaviour
{
    Renderer r;
    MaterialPropertyBlock mpb;

    public int colCount;
    public int rowCount;

    public int currentRow;
    public int currentCol;

    public SpritesheetAnimation[] animations;

    public SpritesheetAnimation currentAnimation;

    public Cooldown frameCooldown = new Cooldown();

    public float debugX;
    public float debugY;
    void Awake()
    {
        r = GetComponent<Renderer>();
        mpb = new MaterialPropertyBlock();
        r.GetPropertyBlock(mpb);
    }

    void Start()
    {
        Play("Front");
    }

    void Update()
    {
        DisplayFrame(currentCol, currentRow);

        if(frameCooldown.UpdateCooldown())
        {
            currentCol++;
            if(currentCol >= currentAnimation.frameDurations.Length)
            {
                if(currentAnimation.playback == SpritesheetAnimationPlayback.Loop)
                {
                    currentCol = 0;
                } else {
                    currentCol = currentAnimation.frameDurations.Length-1;
                }
                
            }

            frameCooldown.Reset(currentAnimation.frameDurations[currentCol]);
        }
    }

    public void Play(string animationName)
    {
        //SpritesheetAnimation animation = animations.FirstOrDefault(a => a.name == animationName);
        SpritesheetAnimation animation = null;

        //Comment trouver un index avec LINQ ?
        for(int i = 0; i < animations.Length; i++)
        {
            if(animations[i].name == animationName)
            {
                currentRow = i;
                animation = animations[i];
            }
        }

        if(animation != null)
        {
            currentAnimation = animation;
            frameCooldown.Stop();
            if(currentAnimation.frameCount > 0)   
            {
                frameCooldown.Reset(currentAnimation.frameDurations[0]);
            }
        }
    }

    public void DisplayCurrentAnimationFrame(int frameIndex)
    {
        if(currentAnimation != null && frameIndex < currentAnimation.frameDurations.Length)
        {
           
            mpb.SetVector("_MainTex_ST", new Vector4(1f, 1f, 0, currentRow / (float)rowCount));	
            r.SetPropertyBlock(mpb);
        }
    }

    public void DisplayFrame(float x, float y)
    {
        mpb.SetVector("_MainTex_ST", new Vector4(1f / (float)colCount, 1f / (float)rowCount, x * (1f / (float)colCount), -y * (1f / rowCount) - (1f / (float)rowCount)));
        r.SetPropertyBlock(mpb);
    }
}
